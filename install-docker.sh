apt-get update

apt-get -y -q install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

apt-get update

apt-get -y -q  install docker-ce docker-ce-cli containerd.io

echo "### Testing that docker can run as sudo inside Ubuntu instance"
docker run hello-world

groupadd docker

usermod -aG docker $USER

systemctl enable docker.service
systemctl enable containerd.service

echo '
{"hosts": ["tcp://0.0.0.0:2375", "unix:///var/run/docker.sock"]}' | tee /etc/docker/daemon.json > /dev/null

mkdir -p /etc/systemd/system/docker.service.d/

echo '[Service]
ExecStart=
ExecStart=/usr/bin/dockerd' | tee /etc/systemd/system/docker.service.d/override.conf > /dev/null

systemctl daemon-reload

systemctl restart docker.service

