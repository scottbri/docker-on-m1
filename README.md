# docker-on-m1

Run Docker on Apple M1 chips using Canonical Multipass Ubuntu image as a service and the docker cli

## Getting started

## Installation
This installation assumes that [homebrew](https://brew.sh/) is already installed.
The script will fully automate the installation of docker, docker-compose, and multipass using homebrew.

We will use the name dockervm for the multipass ubuntu vm instance name.

## Usage
bash -c "$(curl -fsSL https://gitlab.com/scottbri/docker-on-m1/-/raw/main/install.sh)"

## Support
Create an issue in the project, and someone will be with you shortly.

## Authors and acknowledgment
Thanks to the Homebrew, Docker, and Canonical teams.
