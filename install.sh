INSTANCE="${1:-dockervm}"

brew install docker docker-compose multipass
multipass launch -n $INSTANCE

echo "#### Installing Docker in $INSTANCE Ubuntu image"
curl -fsSL https://gitlab.com/scottbri/docker-on-m1/-/raw/main/install-docker.sh > install-docker.sh
multipass transfer  install-docker.sh $INSTANCE:/tmp/
rm install-docker.sh

multipass exec $INSTANCE -- sudo bash /tmp/install-docker.sh

export DOCKER_HOST="$(multipass info $INSTANCE | awk '/IPv4:/ {print $2}'):2375"
echo "#### Setting DOCKER_HOST=$DOCKER_HOST"

echo "### TESTING DOCKER RUN"
docker run hello-world

echo "Add the following command to your .zshrc or .bashrc setting the DOCKER_HOST automatically:"
echo "INSTANCE=$INSTANCE; export DOCKER_HOST=\"\$(multipass info \$INSTANCE | awk '/IPv4:/ {print \$2}'):2375\""
